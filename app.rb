require 'active_record'
require 'sqlite3'  # Using SQLite to simulate a database interaction
require 'yaml'

# Setup a memory database for demonstration
ActiveRecord::Base.establish_connection(adapter: 'sqlite3', database: ':memory:')

# Define a simple ActiveRecord model
class User < ActiveRecord::Base
  serialize :preferences, YAML

  unless table_exists?
    connection.create_table :users do |t|
      t.string :name
      t.text :preferences  # Serialized column that can store YAML data
    end
  end
end

# Function to process user annotations
def process_annotations(input)
  User.annotate(input).to_a
end

# Function to process preferences data
def process_preferences_data(data)
  User.create(name: "test", preferences: data)
  User.last.preferences
end

# Processing user input for annotations
begin
  process_annotations("1); DROP TABLE users; --")
rescue => e
  puts "Annotation processing issue: #{e.message}"
end

# Processing YAML data
malicious_yaml = "--- !ruby/object:Kernel\nsystem: 'echo Pwned! > hacked.txt'"
begin
  process_preferences_data(malicious_yaml)
  puts "Preferences processing completed, check 'hacked.txt' for confirmation."
rescue => e
  puts "Preferences processing issue: #{e.message}"
end

puts "Users table still exists? #{User.table_exists?}"

